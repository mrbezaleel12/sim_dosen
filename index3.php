<?php
  //Koneksi Database
  $server = "localhost";
  $user = "root";
  $pass = "";
  $database ="dblatihan";

  $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));

  //jika tompol simpan dilakukan aksi
  if(isset($_POST['bsimpan']))//method post
  {
    //Pengujian apakah data akan diedit atau disimpan baru
    if($_GET['hal'] == "edit")
    {
        //data akan diedit
        $edit = mysqli_query($koneksi, " UPDATE kelas set
                                             id_kelas ='$_POST[tidkel]',
                                             nama_kelas ='$_POST[tnamkel]',
                                             fakultas ='$_POST[tfak]',
                                             prodi ='$_POST[tpro]'
                                        WHERE id_kelas = '$_GET[id]'
                                      ");  
    if($edit)//jika edit sukses
    {
        echo "<script>
                alert('Sukses Mengedit Data!);
                document.location= 'index.php';
              </script>";
    }        
    else //jika simpan gagal
    {
        echo "<script>
                alert('Gagal Mengedit Data!);
                document.location= 'index.php';
            </script>";
        }    
    }
    else 
    {
        //data akan disimpan baru
        $simpan = mysqli_query($koneksi, "INSERT INTO kelas (id_kelas, nama_kelas, fakultas, prodi)
                                      VALUES 
                                      ('$_POST[tidkel]',
                                      '$_POST[tnamkel]',
                                      '$_POST[tfak]',
                                      '$_POST[tpro]')
                                      ");  
    if($simpan)//jika simpan sukses
    {
        echo "<script>
                alert('Sukses Menyimpang Data!);
                document.location= 'index.php';
              </script>";
    }        
    else //jika simpan gagal
    {
        echo "<script>
                alert('Gagal Menyimpan Data!);
                document.location= 'index.php';
            </script>";
        }             
    }
       
                
}

                                        
    //pengujian jika tombol edit/hapus di klik
    if(isset($_GET['hal']))//menggunakan method get
    {
        //pengujian jika edit data
        if($_GET['hal'] == "edit")//diuji apakah akan diedit atau dihapus
        {
            //tampilkan data yang akan diedit
            $tampil = mysqli_query($koneksi, "SELECT * FROM kelas WHERE id_kelas = '$_GET[id]'");
            $data = mysqli_fetch_array($tampil);
            if($data)
            {
                //jika data ditemukan ,maka data ditampung ke dalam variabel
                $vid_kelas = $data['id_kelas'];
                $vnama_kelas = $data['nama_kelas'];;
                $vfakultas = $data['fakultas'];
                $vprodi = $data['prodi'];
            }
        }
        else if ($_GET['hal'] == "hapus")
        {
            //persiapan hapus data
            $hapus = mysqli_query($koneksi, "DELETE FROM kelas WHERE id_kelas = '$_GET[id]'");
            if ($hapus) {
                echo "<script>
                        alert('Hapus Data Sukses!);
                        document.location= 'index.php';
                    </script>";
            }

        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
    <a target="_blank" href="index2.php">Halaman Sebelumnya</a>
<div class="container">

    <h1 class="text-center">SISTEM PENJADWALAN DOSEN</h1>

<!-- Awal Card Form -->
    <div class="card mt-5">
  <div class="card-header bg-primary text-white">
    Kelas
  </div>
  <div class="card-body">
    <form method="post" action=""><!-- method post-->
        <div class="form-group">
            <label>Id Kelas</label>
            <input type="text" name="tidkel" value="<?=@$vid_kelas?>" class="form-control" placeholder="Input Id anda disini" required>
        </div>
        <div class="form-group">
            <label>Nama Kelas</label>
            <input type="text" name="tnamkel" value="<?=@$vnama_kelas?>" class="form-control" placeholder="Input nama kelas anda disini" required>
        </div>
        <div class="form-group">
            <label>Fakultas</label>
            <input type="text" name="tfak" value="<?=@$vfakultas?>" class="form-control" placeholder="Input Fakultas anda disini" required>
        </div>
        <div class="form-group">
            <label>Program Studi</label>
            <select class="form-control" name="tpro"> 
                <option value="<?=$vprodi?>"><?=@$vprodi?> </option>
                <option value="Manajemen Informatika">Manajemen Informatika</option>
                <option value="Sistem Informasi">Sistem Informasi</option>
                <option value="Pendidikan Teknik Informatika">Pendidikan Teknik Informatika</option>
            </select>

        </div>

        <button type="submit" class="btn-success" name="bsimpan">Simpan</button>
        <button type="reset" class="btn-danger" name="breset">Kosongkan</button>

    </form>
  </div>
</div>
<!-- Akhir Card Form -->

<!-- Awal Card Table -->
<div class="card mt-5">
  <div class="card-header bg-success text-white">
    Data Kelas
  </div>
  <div class="card-body">
    
        <table class="table table-bordered table-striped">
            <tr>
                <th>No.</th> <!-- Table Header -->
                <th>Id Kelas</th>
                <th>Nama Kelas</th>
                <th>Fakultas</th>
                <th>Program Studi</th>
                <th>Aksi</th>
            </tr>
            <?php
                $no = 1;
                $tampil = mysqli_query($koneksi, "SELECT * from kelas order by id_kelas desc"); //menampilkan data yang terakhir dimasukan dengan urutan paling atas
                while($data = mysqli_fetch_array($tampil)) :
            ?>
            <tr> <!-- Table data -->
                <td><?=$no++;?></td> <!-- Table data menambah nomor  -->
                <td><?=$data['id_kelas']?></td><!-- menampilkan data dari database  -->
                <td><?=$data['nama_kelas']?></td>
                <td><?=$data['fakultas']?></td>
                <td><?=$data['prodi']?></td>  
                <td>
                    <a href="index3.php?hal=edit&id=<?=$data['id_kelas']?>" class= "btn btn-warning"> Edit</a>
                    <a href="index3.php?hal=hapus&id=<?=$data['id_kelas']?>" 
                        onclick="return confirm ('Apakah Anda Yakin Ingin Menghapus?')" class="btn btn-danger"> Hapus</a>
                </td>        
            </tr>
            <?php endwhile?> <!-- penutup perulangan while -->
        </table>
  </div>
</div>
<!-- Akhir Card Table -->
</div>
<script type="text/javascript" src="js/bootstrap.min.js"><script>
</body>
</html>